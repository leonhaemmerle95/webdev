from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Hello</p>"

@app.route("/hallo/<word>/")
def hello():
    return "Hallo" + word 

app.run(debug=True)